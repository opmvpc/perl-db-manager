#!/usr/bin/perl

package HtmlHelpers;

# Récupère le code html du nav et l'affiche
sub set_nav_bar {
	my $template_layouts_header = 'templates/layouts/header.html';
	my $active_url = @_[0];

	my $buffer;
	open(TEMPLATE, $template_layouts_header);
	while(<TEMPLATE>) {
	  $buffer .= $_;
	}
	close(TEMPLATE);
	$buffer =~ s/<!--cgi:is_active_$active_url-->/active/g;
	print $buffer;
}

# Récupère le contenu d'un fichier html et l'affiche
sub set_content {
  my $template = @_[0];
  my %errors = %{@_[1]};
  my @old_inputs = @{@_[2]};
  my $buffer;
  open(TEMPLATE, $template);
  while(<TEMPLATE>) {
    $buffer .= $_;
  }
  close(TEMPLATE);

  print $buffer;
}

# Récupère le contenu d'un fichier html et l'affiche
sub set_content_create_table {
  my $template = @_[0];
  my %errors = %{@_[1]};
  my @old_inputs = @{@_[2]};
  my $table = @_[3];

  my $buffer;
  open(TEMPLATE, $template);
  while(<TEMPLATE>) {
    $buffer .= $_;
  }
  close(TEMPLATE);
  if (%errors)
  {
  	#construire form
  	my $html = "";
  	my $size = scalar @old_inputs;
  	# print $size;
  	for (my $i = 0; $i < $size; $i++) {
  		my @old_input = @{@old_inputs[$i]};
  		$html .= '<div class="row">';
		$html .= '<div class="col-sm-4 form-group <!--cgi:has_error_'.$i.'_name-->">';
		$html .= '<input value="'.@old_input[0].'" id="champ" name="champ[]" type="text" class="form-control" placeholder="Nom du champs" required>';
		$html .= '<span class="help-block"><!--cgi:error_'.$i.'_name--></span>';
		$html .= '</div>';
		$html .= '<div class="visible-xs">';
		$html .= '<br>';
		$html .= '</div>';
		$html .= '<div class="col-sm-4">';
		$html .= '<select id="type" name="type[]" class="form-control type'.$i.'" required>';
		$html .= '<option value="">Type du champs</option>';
		$html .= '<option value="INT" '. ((@old_input[1] eq "INT") ? "selected" : "") .' >INT</option>';
		$html .= '<option value="VARCHAR" '. ((@old_input[1] eq "VARCHAR") ? "selected" : "") .' >VARCHAR</option>';
		$html .= '<option value="DATE" '. ((@old_input[1] eq "DATE") ? "selected" : "") .' >DATE</option>';
		$html .= '</select>';
		$html .= '</div>';
		$html .= '<div class="visible-xs">';
		$html .= '<br>';
		$html .= '</div>';
		$html .= '<div  class="col-sm-4 form-group <!--cgi:has_error_'.$i.'_lenght-->">';
		$html .= '<input value="'.@old_input[2].'" id="longueur" name="longueur[]" type="text" class="form-control longueur'.$i.'" placeholder="Longueur du champs" required>';
		$html .= '<span class="help-block"><!--cgi:error_'.$i.'_lenght--></span>';
		$html .= '</div>';
		$html .= '</div>';
 	}
	$buffer =~ s/<!--cgi:row_form-->/$html/g;
	
  	# afficher les erreurs
  	foreach my $key (keys %errors) {
	  	# my $has_error = "<!--cgi:has_error_$key-->";
  		$buffer =~ s/<!--cgi:has_$key-->/has-error/g;
  		$buffer =~ s/<!--cgi:$key-->/$errors{$key}/g;
  	}
  	$buffer =~ s/<!--cgi:table_name-->/$table/g;
  }
  else
  {
  	my $html = "";
	$html .= '<div class="row">';
	$html .= '<div class="col-sm-4 form-group <!--cgi:has_error_0_name-->">';
	$html .= '<input id="champ" name="champ[]" type="text" class="form-control" placeholder="Nom du champs" required>';
	$html .= '<span class="help-block"><!--cgi:error_0_name--></span>';
	$html .= '</div>';
	$html .= '<div class="visible-xs">';
	$html .= '<br>';
	$html .= '</div>';
	$html .= '<div class="col-sm-4">';
	$html .= '<select id="type" name="type[]" class="form-control type0" required>';
	$html .= '<option value="" selected>Type du champs</option>';
	$html .= '<option value="INT">INT</option>';
	$html .= '<option value="VARCHAR">VARCHAR</option>';
	$html .= '<option value="DATE">DATE</option>';
	$html .= '</select>';
	$html .= '</div>';
	$html .= '<div class="visible-xs">';
	$html .= '<br>';
	$html .= '</div>';
	$html .= '<div  class="col-sm-4 form-group <!--cgi:has_error_0_lenght-->">';
	$html .= '<input id="longueur" name="longueur[]" type="text" class="form-control longueur0" placeholder="Longueur du champs" required>';
	$html .= '<span class="help-block"><!--cgi:error_0_lenght--></span>';
	$html .= '</div>';
	$html .= '</div>';
	$buffer =~ s/<!--cgi:row_form-->/$html/g;
	$buffer =~ s/<!--cgi:table_name-->//g;
  }
  print $buffer;
}

# Afficher la liste des tables
sub set_table_html {
	my $template_tables = 'templates/table_list.html';
	my @table_names = @{@_[0]};
	my $html = "";
	my $i = 1;
	foreach my $name (@table_names) {
		$html .= "<tr>";
		$html .= "<td>";
		$html .= $i++;
		$html .= "</td>";
		$html .= "<td>";
		$html .= $name;
		$html .= "</td>";
		$html .= "<td>";
		$html .= "<a href='show_table.pl?name=$name' class='btn btn-primary btn-xs'>Afficher</a>";
		$html .= "</td>";
		$html .= "<td>";
		$html .= "<a href='insert_into_table.pl?name=$name' class='btn btn-success btn-xs'>Insérer</a>";
		$html .= "</td>";
		$html .= "<td>";
		$html .= "<a id='$name' class='btn btn-danger btn-xs'>Supprimer</a>";
		$html .= "</td>";
		$html .= "</tr>";
	}

	# Content
	my $buffer;
	open(TEMPLATE, $template_tables);
	while(<TEMPLATE>) {
		$buffer .= $_;
	}
	close(TEMPLATE);
	$buffer =~ s/<!--cgi:table_content-->/$html/g;
	print $buffer;
}


# Afficher la liste des entrées d'une table
sub set_entries_html {
	my $template_table = 'templates/show_table.html';
	my $table = @_[0];
	my @table_infos = @{@_[1]};
	my @table_datas = @{@_[2]};
	my $html = "";

	$html .= "<thead><tr>";
	foreach my $row (@table_infos) {
	  $html .= "<th>@{$row}[0]</th>";
	}
	$html .= "<th style=\"width: 50px\"></th><th style=\"width: 50px\"></th></tr></thead>";

	foreach my $row (@table_datas) {
	  $html .= "<tr>";
	  my @row = @{$row};
	  my $id = @row[0];
	  foreach my $x (@row) {
	    $html .= "<td>";
	    $html .= $x;
	    $html .= "</td>";
	  }
	  $html .= "<td>";
	  $html .= "<a href='edit_row.pl?name=$table&id=$id' class='btn btn-warning btn-xs'>Modifier</a>";
	  $html .= "</td>";
	  $html .= "<td>";
	  $html .= "<a id='row$id' class='btn btn-danger btn-xs'>Supprimer</a>";
	  $html .= "</td>";
	  $html .= "</tr>";	  
	}
	# Content
	my $buffer;
	open(TEMPLATE, $template_table);
	while(<TEMPLATE>) {
	  $buffer .= $_;
	}
	close(TEMPLATE);
	$buffer =~ s/<!--cgi:table_content-->/$html/g;
	$buffer =~ s/<!--cgi:table_name-->/$table/g;
	print $buffer;
}

# Afficher le formulaire
sub set_insert_form_html {
	my $template_form = "templates/insert_into.html";
	my $table = @_[0];
	my @table_infos = @{@_[1]};
	my %errors = %{@_[2]};
  	my %old_inputs = %{@_[3]};
	my $html = "";

	foreach my $col (@table_infos)
	{
		my @col = @{$col};
		unless (@col[0] eq "ID") {
			if (%errors) {
			    $html .= "<div class=\"form-group col-sm-12 <!--cgi:has_error_@col[0]-->\">";
			    $html .= "<label for=\"@col[0]\" class=\"col-sm-3 control-label text-right\" >@col[0] <br class=\"hidden-xs\"><small>(@col[1])</small></label>";
			    $html .= "<div class=\"col-sm-9\">";
			    $html .= "<input value=\"<!--cgi:@col[0]-->\" id=\"@col[0]\" name=\"@col[0]\" type=\"text\" class=\"form-control\" placeholder=\"Valeur du champs @col[0]\" required>";
			    $html .= '<span class="help-block"><!--cgi:error_'.@col[0].'--></span>';
			    $html .= "</div></div>";

		      	# afficher les erreurs
			  	foreach my $key (keys %errors) {
			  		$html =~ s/<!--cgi:has_error_$key-->/has-error/g;
			  		$html =~ s/<!--cgi:error_$key-->/$errors{$key}/g;
			  	}

			  	# afficher les anciennes valeurs
			  	foreach my $key (keys %old_inputs) {
			  		$html =~ s/<!--cgi:$key-->/$old_inputs{$key}/g;
			  	}
			}
			else {
				$html .= "<div class=\"form-group col-sm-12\">";
			    $html .= "<label for=\"@col[0]\" class=\"col-sm-3 control-label text-right\" >@col[0] <br class=\"hidden-xs\"><small>(@col[1])</small></label>";
			    $html .= "<div class=\"col-sm-9\">";
			    $html .= "<input id=\"@col[0]\" name=\"@col[0]\" type=\"text\" class=\"form-control\" placeholder=\"Valeur du champs @col[0]\" required>";
			    $html .= "</div></div>";
			}
		}
	} 

	# Content
	my $buffer;
	open(TEMPLATE, $template_form);
	while(<TEMPLATE>) {
	  $buffer .= $_;
	}
	close(TEMPLATE);
	$buffer =~ s/<!--cgi:form-content-->/$html/g;
	$buffer =~ s/<!--cgi:table_name-->/$table/g;
	print $buffer;
}

# Afficher le formulaire
sub set_update_form_html {
	my $template_row = "templates/edit_row.html";
	my $table = @_[0];
	my @table_infos = @{@_[1]};
	my @row_datas = @{@_[2]};
	my %errors = %{@_[3]};
  	my %old_inputs = %{@_[4]};
	my $html = "";
	my $i = 0;
	my $id= @row_datas[$i];

	foreach my $champs (@table_infos) {
		my @champ = @{$champs};
		if (%errors) {
			$html .= "<div class=\"form-group col-sm-12 <!--cgi:has_error_@champ[0]-->\">";
			$html .= "<label for='@champ[0]' class='col-sm-3 control-label text-right' >@champ[0] <br class='hidden-xs'><small>(@champ[1])</small></label>";
			$html .= "<div class='col-sm-9'>";
			$html .= "<input value='<!--cgi:@champ[0]-->' id='@champ[0]' name='@champ[0]' type='text' class='form-control' placeholder='Valeur du champs @champ[0]' required ";
			$html .= (@champ[0] eq "ID") ? "disabled>" : ">";
			$html .= '<span class="help-block"><!--cgi:error_'.@champ[0].'--></span>';
			$html .= "</div></div>";

			# afficher les erreurs
			foreach my $key (keys %errors) {
				$html =~ s/<!--cgi:has_error_$key-->/has-error/g;
				$html =~ s/<!--cgi:error_$key-->/$errors{$key}/g;
				$html =~ s/<!--cgi:ID-->/$id/g;
			}

			# afficher les anciennes valeurs
			foreach my $key (keys %old_inputs) {
				$html =~ s/<!--cgi:$key-->/$old_inputs{$key}/g;
			}
		}
		else {
			$html .= "<div class=\"form-group col-sm-12\">";
			$html .= "<label for='@champ[0]' class='col-sm-3 control-label text-right' >@champ[0] <br class='hidden-xs'><small>(@champ[1])</small></label>";
			$html .= "<div class='col-sm-9'>";
			$html .= "<input value='@row_datas[$i++]' id='@champ[0]' name='@champ[0]' type='text' class='form-control' placeholder='Valeur du champs @champ[0]' required ";
			$html .= (@champ[0] eq "ID") ? "disabled>" : ">";
			$html .= "</div></div>";
		}
	}

	# Content
	my $buffer;
	open(TEMPLATE, $template_row);
	while(<TEMPLATE>) {
	  $buffer .= $_;
	}
	close(TEMPLATE);
	$buffer =~ s/<!--cgi:form_content-->/$html/g;
	$buffer =~ s/<!--cgi:table_name-->/$table/g;
	$buffer =~ s/<!--cgi:id-->/$id/g;
	print $buffer;
}

sub set_session_message_html {
	my $message = @_[0];
	print "<div class=\"col-lg-offset-3 col-lg-6\"><div class=\"alert alert-dismissible alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button><h4>Succès !</h4><p>$message</p></div></div>";
}

sub set_session_message_error_html {
	my $html = "<div class=\"col-lg-offset-3 col-lg-6\"><div class=\"alert alert-dismissible alert-warning\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button><h4>";
	$html .= "Attention !</h4><p>Veuillez vérifier les informations que vous avez encodées puis réessayez.</p></div></div>";
	print $html;
}

1;
