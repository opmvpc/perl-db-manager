#! /usr/bin/perl

use CGI qw(-any);
use strict;
use DBManager;
use CGI::Session;

my $page = CGI->new;

# Récupération des parametres recu en get et supression de l'entrée
my $table = $page->param("name");
my $id = $page->param("id");

my $session = new CGI::Session(undef, $page, {Directory=>'sessions'});
$session->param("deleted", "L'entrée #$id a été supprimée !");

DBManager::delete_row($table, $id);

my $cookie = $page->cookie( -name => $session->name, -value => $session->id );
print $page->redirect(
             -location => "show_table.pl?name=$table",
             -cookie => $cookie
            );