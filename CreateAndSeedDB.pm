#!/usr/bin/perl

package CreateAndSeedDB;

use DBI;
use strict;

my $driver   = "SQLite"; 
my $database = "dbmanager.db";
my $dsn = "DBI:$driver:$database";
my $userid = "";
my $password = "";

sub connect_to_db {
	my $dbh = DBI->connect($dsn, $userid, $password, { RaiseError => 1 }) 
   	or die $DBI::errstr;

	print "Opened database successfully\n";

	return $dbh
}

sub disconnect_db {
	@_[0]->disconnect();
}

sub create_table_and_seed {
	my $dbh = connect_to_db();

	#ne marche pas si déja créée
	# create_table($dbh);

	# seed_db($dbh);

	show_datas($dbh);

	disconnect_db($dbh);
} 

sub create_table {
	my $stmt = qq(CREATE TABLE COMPANY
   		(ID INT PRIMARY KEY     NOT NULL,
		NAME           TEXT    NOT NULL,
		AGE            INT     NOT NULL,
		ADDRESS        CHAR(50),
		SALARY         REAL););

	my $rv = @_[0]->do($stmt);

	if($rv < 0) {
	   print $DBI::errstr;
	} else {
	   print "Table created successfully\n";
	}
}

sub seed_db {
	my $stmt = qq(INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)
               VALUES (1, 'Paul', 32, 'California', 20000.00 ));
	my $rv = @_[0]->do($stmt) or die $DBI::errstr;

	$stmt = qq(INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)
	               VALUES (2, 'Allen', 25, 'Texas', 15000.00 ));
	$rv = @_[0]->do($stmt) or die $DBI::errstr;

	$stmt = qq(INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)
	               VALUES (3, 'Teddy', 23, 'Norway', 20000.00 ));

	$rv = @_[0]->do($stmt) or die $DBI::errstr;

	$stmt = qq(INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)
	               VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 ););

	$rv = @_[0]->do($stmt) or die $DBI::errstr;

	print "Records created successfully\n";
}

sub show_datas {
	my $stmt = qq(SELECT ID, NAME, ADDRESS, SALARY from COMPANY;);
	my $sth = @_[0]->prepare( $stmt );
	my $rv = $sth->execute() or die $DBI::errstr;

	if($rv < 0) {
	   print $DBI::errstr;
	}

	while(my @row = $sth->fetchrow_array()) {
	      print "ID = ". $row[0] . "\n";
	      print "NAME = ". $row[1] ."\n";
	      print "ADDRESS = ". $row[2] ."\n";
	      print "SALARY =  ". $row[3] ."\n\n";
	}
	print "Operation done successfully\n";

}

1;