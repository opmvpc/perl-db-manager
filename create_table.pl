#! /usr/bin/perl

use CGI qw(-any);
use strict;
use Helpers::HtmlHelpers;
use CGI::Session;

# Template paths
my $template_js = "js/create_table_errors.js";
my $fichier_js = "js/create_errors.js";

my $page = CGI->new;

my $session = new CGI::Session(undef, $page, {Directory=>'sessions'});

my $ref_errors = $session->param("errors");
my $ref_old_inputs = $session->param("old_inputs");
my $old_inputs_size = ($ref_old_inputs) ? scalar @{$ref_old_inputs} : 0;
my $table = $session->param("table");

my $cookie = $page->cookie( -name => $session->name, -value => $session->id );

print $page->header(
             -charset=>'utf-8',
             -cookie => $cookie
            );
set_html_head();
HtmlHelpers::set_nav_bar("create_table");
set_session_message_error();
HtmlHelpers::set_content_create_table("templates/create_table.html", $ref_errors, $ref_old_inputs, $table);
$session->clear(["old_inputs"]);
print $page->end_html();

set_js();
sub set_html_head {
  print $page->start_html(-title=>"DB Manager | Créer une table",
                          -head=>$page->meta({-name => 'viewport',-content=>'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1'}),
                          -style => { 'src' => ['styles/bootstrap.min.css', 'styles/custom.css', 'styles/sweetalert.css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'] },
                          -script=>[
                            { -type => 'text/javascript',
                              -src  => 'js/jquery.js'
                            },
                            { -type => 'text/javascript',
                              -src  => 'js/bootstrap.min.js'
                            },
                            { -type => 'text/javascript',
                              -src  => 'js/create_table.js'
                            },
                            { -type => 'text/javascript',
                              -src  => $fichier_js
                            }
                          ]
                        );
}

sub set_session_message_error {
  # Gestion des messages en session
  if ($session->param("errors"))
  {
    HtmlHelpers::set_session_message_error_html();
    $session->clear(["errors"]);
  }
} 

# Générer javascript pour les champs générés après erreurs
sub set_js {
  my $buffer;
  open(JS, $template_js);
  while(<JS>) {
    $buffer .= $_;
  }
  close(JS);

  # Si pas d'erreurs, js pour le formulaire de base
  unless ($ref_errors) {
    $old_inputs_size = 1;
  }

  open (FILEHANDLE, ">", $fichier_js) or die "Impossible d'ouvrir le fichier: $!";
  print FILEHANDLE "\$( document ).ready(function() {\n";
  for (my $i=0; $i<$old_inputs_size; $i++) {
    my $code = $buffer;
    $code =~ s/<!--cgi:compteur-->/$i/g;
    print FILEHANDLE $code;
  }
  print FILEHANDLE "\n});";
  close(FILEHANDLE);

  my $buffer;
  open(JS, 'js/create_table_template.js');
  while(<JS>) {
    $buffer .= $_;
  }
  close(JS);

  open (FILEHANDLE, ">", 'js/create_table.js') or die "Impossible d'ouvrir le fichier: $!";
  my $code = $buffer;
  $code =~ s/<!--cgi:compteur-->/$old_inputs_size+1/g;
  print FILEHANDLE $code;
  close(FILEHANDLE);
}