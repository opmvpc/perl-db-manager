#! /usr/bin/perl

use CGI qw(-any);
use strict;
use DBManager;
use CGI::Session;

my $page = CGI->new;

# Récupération des parametres recu en get et supression de la table
my $table = $page->param("name");

my $session = new CGI::Session(undef, $page, {Directory=>'sessions'});
$session->param("deleted", "La table \"$table\" a été supprimée !");

DBManager::delete_table($table);

my $cookie = $page->cookie( -name => $session->name, -value => $session->id );
print $page->redirect(
             -location => 'tables.pl',
             -cookie => $cookie
            );