document.querySelector("#<!--cgi:table_id-->").onclick = function(){
	swal({
			title: "Êtes-vous certain?",
			text: "<!--cgi:type--> sera supprimée définitivement!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Oui, la supprimer!",
			cancelButtonText: "Non, annuler!",
			closeOnConfirm: false,
			closeOnCancel: false 
		},
		function(isConfirm) {
			if (isConfirm) {
				$(location).attr("href","<!--cgi:delete_action-->");
			}
			else {
				swal("Opération annulée", "<!--cgi:type--> n'a pas été supprimée :)", "error");
			}
		}
	);
};