var $type<!--cgi:compteur--> = $('.type<!--cgi:compteur-->'),
$longueur<!--cgi:compteur--> = $('.longueur<!--cgi:compteur-->');
$type<!--cgi:compteur-->.change(function() {
    if ($type<!--cgi:compteur-->.val() == 'VARCHAR') {
        $longueur<!--cgi:compteur-->.removeAttr('disabled');
    } else {
        $longueur<!--cgi:compteur-->.attr('disabled', 'disabled').val('');
    }
}).trigger('change');
