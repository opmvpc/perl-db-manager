$( document ).ready(function() {
document.querySelector("#animaux").onclick = function(){
	swal({
			title: "Êtes-vous certain?",
			text: "La table sera supprimée définitivement!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Oui, la supprimer!",
			cancelButtonText: "Non, annuler!",
			closeOnConfirm: false,
			closeOnCancel: false 
		},
		function(isConfirm) {
			if (isConfirm) {
				$(location).attr("href","cible_delete_table.pl?name=animaux");
			}
			else {
				swal("Opération annulée", "La table n'a pas été supprimée :)", "error");
			}
		}
	);
};document.querySelector("#Entrées").onclick = function(){
	swal({
			title: "Êtes-vous certain?",
			text: "La table sera supprimée définitivement!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Oui, la supprimer!",
			cancelButtonText: "Non, annuler!",
			closeOnConfirm: false,
			closeOnCancel: false 
		},
		function(isConfirm) {
			if (isConfirm) {
				$(location).attr("href","cible_delete_table.pl?name=Entrées");
			}
			else {
				swal("Opération annulée", "La table n'a pas été supprimée :)", "error");
			}
		}
	);
};document.querySelector("#test").onclick = function(){
	swal({
			title: "Êtes-vous certain?",
			text: "La table sera supprimée définitivement!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Oui, la supprimer!",
			cancelButtonText: "Non, annuler!",
			closeOnConfirm: false,
			closeOnCancel: false 
		},
		function(isConfirm) {
			if (isConfirm) {
				$(location).attr("href","cible_delete_table.pl?name=test");
			}
			else {
				swal("Opération annulée", "La table n'a pas été supprimée :)", "error");
			}
		}
	);
};
});