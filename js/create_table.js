$( document ).ready(function() {
	// Ajouter une ligne au formulaire
	var compteur = 1+1;
	$("#add_row").click( function () {
	  	compteur++;
	    var table_row = '<hr><div class="row"><div class="col-sm-4"><input id="champ" name="champ[]" type="text" class="form-control" placeholder="Nom du champs" required></div><div class="visible-xs"><br></div><div class="col-sm-4"><select id="type" name="type[]" class="form-control type'+compteur+'" required><option value="" selected>Type du champs</option><option value="INT">INT</option><option value="VARCHAR">VARCHAR</option><option value="DATE">DATE</option></select></div><div class="visible-xs"><br></div><div  class="col-sm-4"><input id="longueur" name="longueur[]" type="text" class="form-control longueur'+compteur+'" placeholder="Longueur du champs" required></div></div>';
		// var newDiv = $("<div>").append(someText).click(function () { alert("click!"); });
	    $("#rows").append(table_row);
	     
		// Désactiver le champ longueur si type != de VARCHAR
	    var $type = $('.type'+compteur),
	    $longueur = $('.longueur'+compteur);

		$type.change(function() {
		    if ($type.val() == 'VARCHAR') {
		        $longueur.removeAttr('disabled');
		    } else {
		        $longueur.attr('disabled', 'disabled').val('');
		    }
		}).trigger('change');

	});

	// Désactiver le champ longueur si type != de VARCHAR
	var $type = $('.type'),
	$longueur = $('.longueur');
	$type.change(function() {
	    if ($type.val() == 'VARCHAR') {
	        $longueur.removeAttr('disabled');
	    } else {
	        $longueur.attr('disabled', 'disabled').val('');
	    }
	}).trigger('change');
});