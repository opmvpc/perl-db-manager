#! /usr/bin/perl

use CGI qw(-any);
use strict;
use DBManager;
use CGI::Session;
use Switch;

my $page = CGI->new;

my %form_datas;
my @keys = $page->param();
foreach my $key (@keys) {
    $form_datas{$key} = $page->param($key);
}

my $table = $page->param('table_name');

my $has_error = validate_form();
my $redirect;

my $session = new CGI::Session(undef, $page, {Directory=>'sessions'});
my $flag_error = 0;
unless ($has_error) {
	$flag_error = 1;
	$session->param("inserted", "La nouvelle entrée a été insérée !");
	$redirect = "show_table.pl?name=$table";  
}
else {
	$session->param("errors", $has_error);
	$session->param("old_inputs", \%form_datas);
	$session->param("table", $table);
	$redirect = "insert_into_table.pl?name=$table";
}

my $cookie = $page->cookie( -name => $session->name, -value => $session->id );
print $page->redirect(
             -location => $redirect,
             -cookie => $cookie
            );

if ($flag_error) {
	DBManager::insert_into(\%form_datas);
}

sub validate_form {
	my %errors;
	my @table_infos = DBManager::get_table_infos($table);

	for my $key (keys %form_datas) 	{
		for my $ref_info (@table_infos) {
			if ($key eq @{$ref_info}[0]) {
				switch (@{$ref_info}[1]) {
					case "INT"	 	{ 
						unless ($form_datas{$key} =~ m/\d+/) {
							$errors{$key} = "Le champ $key doit être un nombre entier";
						}
					}
					case m/CHAR/	{
						my $char_size = substr @{$ref_info}[1], 5;
						$char_size =~ s/\Q)\E//g;
						if (length($form_datas{$key}) > $char_size) {
							$errors{$key} = "Le champ $key doit avoir une longueur de maximum $char_size caractères";
						}
					}
					case "DATE"		{
						unless ($form_datas{$key} =~ m!^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$! ) {
							$errors{$key} = "Le champ $key doit respecter le format AAAA-MM-JJ";
						}
					}
				}
			}
		}
	}

	if (%errors) {
		return \%errors;
	}
	else {
		return 0;
	}
}