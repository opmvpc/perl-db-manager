#! /usr/bin/perl

use CGI qw(-any);
use strict;
use DBManager;
use CGI::Session;
use Helpers::HtmlHelpers;

# Template paths
my $template_js = "js/tables_sweetalerts_template.js";
my $fichier_js = "js/tables_sweetalerts.js";

my $page = CGI->new;

my $table = $page->param("name");
my @table_infos = DBManager::get_table_infos($table);
my @table_datas = DBManager::get_table_datas($table);

my $session = new CGI::Session(undef, $page, {Directory=>'sessions'});
my $cookie = $page->cookie( -name => $session->name, -value => $session->id );
print $page->header(
             -charset=>'utf-8',
             -cookie => $cookie
            );
set_js();
set_html_head();
HtmlHelpers::set_nav_bar("tables");
set_session_message();
HtmlHelpers::set_entries_html($table, \@table_infos, \@table_datas);
print $page->end_html();


sub set_html_head {
  print $page->start_html(-title=>"DB Manager | Table $table",
                          -head=>$page->meta({-name => 'viewport',-content=>'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1'}),
                          -style => { 'src' => ['styles/bootstrap.min.css', 'styles/custom.css', 'styles/sweetalert.css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'] },
                          -script=>[
                            { -type => 'text/javascript',
                              -src  => 'js/jquery.js'
                            },
                            { -type => 'text/javascript',
                              -src  => 'js/bootstrap.min.js'
                            },
                            { -type => 'text/javascript',
                              -src  => 'js/sweetalert.js'
                            },
                            { -type => 'text/javascript',
                              -src      => $fichier_js
                            },
                          ]
                        );
}

sub set_session_message {
  # Gestion des messages en session
  if ($session->param("inserted"))
  {
    HtmlHelpers::set_session_message_html ($session->param('inserted'));
    $session->clear(["inserted"]);
  }
  if ($session->param("deleted"))
  {
    HtmlHelpers::set_session_message_html ($session->param('deleted'));
    $session->clear(["deleted"]);
  }
  if ($session->param("edited"))
  {
    HtmlHelpers::set_session_message_html ($session->param('edited'));
    $session->clear(["edited"]);
  }
} 

# Générer javascript des sweetalerts pour la supression de la table ou d'une entrée de la table
sub set_js {
  my $buffer;
  open(JS, $template_js);
  while(<JS>) {
    $buffer .= $_;
  }
  close(JS);

  # Générer javascript des sweetalerts pour la supression d'une table
  open (FILEHANDLE, ">", $fichier_js) or die "Impossible d'ouvrir le fichier: $!";
  print FILEHANDLE "\$( document ).ready(function() {\n";
  my $code = $buffer;
  $code =~ s/<!--cgi:table_id-->/$table/g;
  $code =~ s/<!--cgi:type-->/La table/g;
  $code =~ s/<!--cgi:delete_action-->/cible_delete_table.pl?name=$table/g;
  print FILEHANDLE $code;

  foreach my $row (@table_datas) {
    my $code = $buffer;
    my $id =  @{$row}[0];
    $code =~ s/<!--cgi:table_id-->/row$id/g;
    $code =~ s/<!--cgi:type-->/L'entrée/g;
    $code =~ s/<!--cgi:delete_action-->/cible_delete_row.pl?name=$table&id=$id/g;
    print FILEHANDLE "\n$code\n";
  }

  print FILEHANDLE "\n});";
  close(FILEHANDLE);
}