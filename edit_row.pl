#! /usr/bin/perl

use CGI qw(-any);
use strict;
use DBManager;
use CGI::Session;
use Helpers::HtmlHelpers;

my $page = CGI->new;

my $table = $page->param("name");
my $id = $page->param("id");
my @table_infos = DBManager::get_table_infos($table);
my @row_datas = DBManager::get_row_datas($table, $id);

my $session = new CGI::Session(undef, $page, {Directory=>'sessions'});
my $ref_errors = $session->param("errors");
my $ref_old_inputs = $session->param("old_inputs");
my $cookie = $page->cookie( -name => $session->name, -value => $session->id );
print $page->header(
             -charset=>'utf-8',
             -cookie => $cookie
            );
set_html_head();
HtmlHelpers::set_nav_bar("tables");
set_session_message_error();
HtmlHelpers::set_update_form_html($table, \@table_infos, \@row_datas, $ref_errors, $ref_old_inputs);
$session->clear(["old_inputs"]);
print $page->end_html();


sub set_html_head {
  print $page->start_html(-title=>"DB Manager | Modifier une entrée de la table $table",
                          -head=>$page->meta({-name => 'viewport',-content=>'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1'}),
                          -style => { 'src' => ['styles/bootstrap.min.css', 'styles/custom.css', 'styles/sweetalert.css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'] },
                          -script=>[
                            { -type => 'text/javascript',
                              -src  => 'js/jquery.js'
                            },
                            { -type => 'text/javascript',
                              -src  => 'js/bootstrap.min.js'
                            },
                          ]
                        );
}

sub set_session_message_error {
  # Gestion des messages en session
  if ($session->param("errors"))
  {
    HtmlHelpers::set_session_message_error_html();
    $session->clear(["errors"]);
  }
} 