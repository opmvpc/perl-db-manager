#!/usr/bin/perl

package DBManager;

use DBI;
use strict;

my $driver   = "SQLite"; 
my $database = "dbmanager.db";
my $dsn = "DBI:$driver:$database";
my $userid = "";
my $password = "";

sub connect_to_db {
	my $dbh = DBI->connect($dsn, $userid, $password, { RaiseError => 1 }) 
   	or die $DBI::errstr;

	return $dbh
}

sub disconnect_db {
	@_[0]->disconnect();
}

sub get_table_names {
	my $dbh = connect_to_db();
	
	my $statement = "SELECT name FROM sqlite_master WHERE type='table' AND name <> 'sqlite_sequence';";
 	my $ary_ref = $dbh->selectcol_arrayref($statement);

	disconnect_db($dbh);

	return $ary_ref;
}

sub get_table_infos {
	my $dbh = connect_to_db();

	my $stmt = "pragma table_info('@_');";
	
	my $sth = $dbh->prepare( $stmt );
	my $rv = $sth->execute() or die $DBI::errstr;

	if($rv < 0) {
	   print $DBI::errstr;
	}

	# return $stmt;
	my @datas;
	while(my @row = $sth->fetchrow_array()) {
		my @col = ($row[1], $row[2], $row[3]);
		push @datas, \@col;
	      # print "ID = ". $row[0] . "<br>";
	      # print "NAME = ". $row[1] ."<br>";
	      # print "TYPE = ". $row[2] ."<br>";
	      # print "NOTNULL =  ". $row[3] ."<br><br>";
	}
	# print "Operation done successfully<br>";

	disconnect_db($dbh);

	return @datas;
}

sub add_table {
	my $dbh = connect_to_db();

	my $stmt = "CREATE TABLE @_[0] ";
	$stmt .= "(ID INTEGER PRIMARY KEY AUTOINCREMENT,";

	for (my $i = 1; $i < @_ ; $i++) {
		my @champ = @{@_[$i]};
		$stmt .= (@champ[1] eq 'VARCHAR') ? " @champ[0] CHAR(@champ[2])" : " @champ[0] @champ[1] NOT NULL" ;
		$stmt .= ($i == @_ -1) ? "" : ",";
	}
	$stmt .= ");";

	my $rv = $dbh->do($stmt);
	if($rv < 0) {
	   print $DBI::errstr;
	} else {
	   # print "Table created successfully\n";
	}

	disconnect_db($dbh);
}

sub delete_table {
	my $dbh = connect_to_db();

	my $stmt = "DROP TABLE @_;";

	my $rv = $dbh->do($stmt);
	if($rv < 0) {
	   print $DBI::errstr;
	} else {
	   # print "Table deleted successfully\n";
	}

	disconnect_db($dbh);
}

sub get_table_datas {
	my $dbh = connect_to_db();

	my $stmt = "SELECT * FROM @_[0];";

	my $sth = $dbh->prepare( $stmt );
	my $rv = $sth->execute() or die $DBI::errstr;

	if($rv < 0) {
	   print $DBI::errstr;
	}

	my @datas;
	while(my @row = $sth->fetchrow_array()) {
      	my @col;
      	foreach my $x (@row) {
      		push @col, $x;
      	}
      	push @datas, \@col;
	}

	disconnect_db($dbh);

	return @datas;
}

sub insert_into {
	my $dbh = connect_to_db();

	my %datas = %{@_[0]};
	my $size = keys %datas;

	my $stmt = "INSERT INTO $datas{table_name} (";
	my $i = 0;
	print $size;
	foreach my $key (keys %datas) {
		unless ( $key eq "table_name") {
	  		$stmt .= $key;
	  		$stmt .= ($i == $size -1 ) ? ") VALUES (" : ",";
	  	}
	  	$i++;
	}

	$i = 0;
	foreach my $key (keys %datas) {
		unless ( $key eq "table_name") {
	  		$stmt .= "\'$datas{$key}\'";
	  		$stmt .= ($i == $size -1) ? ")" : ",";
	  	}
	  	$i++;
	}

	print $stmt;
	my $rv = $dbh->do($stmt) or die $DBI::errstr;
	print "Records created successfully\n";
	disconnect_db($dbh);
}

sub delete_row {
	my $dbh = connect_to_db();

	my $stmt = qq(DELETE from @_[0] where ID = @_[1];);
	my $rv = $dbh->do($stmt) or die $DBI::errstr;

	disconnect_db($dbh);
}

sub get_row_datas {
	my $dbh = connect_to_db();

	my $stmt = qq(SELECT * from @_[0] where ID = @_[1];);

	my $sth = $dbh->prepare( $stmt );
	my $rv = $sth->execute() or die $DBI::errstr;

	if($rv < 0) {
	   print $DBI::errstr;
	}

	my @datas;
	while(my @row = $sth->fetchrow_array()) {
      	push @datas, @row;
	}

	disconnect_db($dbh);

	return @datas;
}

sub update_row {
	my $dbh = connect_to_db();
	my %form_datas = %{@_[2]}; 
	my $size = keys %form_datas;

	my $stmt = "UPDATE @_[0] set ";
	my $i = 0;
	foreach my $key (keys %form_datas) {

  		$stmt .= "$key = \'$form_datas{$key}\'";
  		$stmt .= ($i == $size -1) ? "" : ",";

	  	$i++;
	}
	$stmt .= " where ID=@_[1]";

	print $stmt;
	
	my $rv = $dbh->do($stmt) or die $DBI::errstr;

	if( $rv < 0 ) {
	   print $DBI::errstr;
	}

	disconnect_db($dbh);
}


1;