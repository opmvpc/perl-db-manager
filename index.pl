#! /usr/bin/perl

use CGI qw(-any);
use strict;
use Helpers::HtmlHelpers;

my $page = CGI->new;

print $page->header(-charset=>'utf-8',);
set_html_head();
HtmlHelpers::set_nav_bar();
HtmlHelpers::set_content("templates/home.html");
print $page->end_html();


sub set_html_head {
  print $page->start_html(-title=>"DB Manager | Bienvenue",
                          -head=>$page->meta({-name => 'viewport',-content=>'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1'}),
                          -style => { 'src' => ['styles/bootstrap.min.css', 'styles/custom.css', 'styles/sweetalert.css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'] },
                          -script=>[
                            { -type => 'text/javascript',
                              -src  => 'js/jquery.js'
                            },
                            { -type => 'text/javascript',
                              -src  => 'js/bootstrap.min.js'
                            },
                            { -type => 'text/javascript',
                              -src  => 'js/sweetalert.js'
                            },
                          ]
                        );
}