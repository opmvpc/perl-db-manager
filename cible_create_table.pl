#! /usr/bin/perl

use CGI qw(-any);
use strict;
use DBManager;
use CGI::Session;

my $page = CGI->new;

# Récupération des parametres recu en post et création de la table en bdd
my $table = $page->param("table_name");
my $i = 0; my $j = 0;
my @champs;
foreach my $value ($page->param("champ[]")) {
  my @champ;
  push @champ, $value;
  push @champ, ($page->param('type[]'))[$i];
  my $longueur = (($page->param('type[]'))[$i++] eq "VARCHAR") ? ($page->param('longueur[]'))[$j++] : undef;
  push @champ, $longueur;
  push @champs, \@champ;
}

# Gestion des erreurs
my $session = new CGI::Session(undef, $page, {Directory=>'sessions'});
# print $page->header;
my $has_error = validate_form();
my $redirect;

# my $cookie = $page->cookie( -name => $session->name, -value => $session->id );
unless ($has_error) {
  DBManager::add_table($table, @champs);
  $session->param("created", "La table \"$table\" a été créée !");
  $redirect = 'tables.pl';
}
else {
  $session->param("errors", $has_error);
  $session->param("old_inputs", \@champs);
  $session->param("table", $table);
  $redirect = 'create_table.pl';
}

my $cookie = $page->cookie( -name => $session->name, -value => $session->id );
print $page->redirect(
             -location => $redirect,
             -cookie => $cookie
            );


sub validate_form {
  my %errors;
	if( $table =~ m/\s/ ) { 
    $errors{'error_table_name'} = 'Le nom de la table ne peut pas contenir d\'espace';
	}

  my @table_names = @{DBManager::get_table_names()};
  foreach my $name (@table_names) {
    if ($name eq $table) {
      $errors{'error_table_name'} = "La table \"$table\" existe déjà";
    }
  }

  my $size = scalar @champs;
  for (my $i = 0; $i < $size; $i++) {
    my @champ = @{@champs[$i]};
      if ( @champ[0] =~ m/\s/ ){
        $errors{"error_".$i."_name"} = "Le nom du champ ne peut pas contenir d\'espace";
      }
      if ( @champ[1] eq "VARCHAR" ){
        if ( @champ[2] =~ m/\D/ ) {
          $errors{"error_".$i."_lenght"} = "La longueur du champ doit être un nombre entier";
        }
      }
    my $flag = 0;
    for (my $j = 0; $j < $size; $j++) {
      my @c = @{@champs[$j]};
      if (($i != $j) and (@c[0] eq @champ[0]) and ($flag == 1)) {
        $errors{"error_".$j ."_name"} = "Deux champs ne peuvent pas avoir le même nom";
        $flag = 0;
      }
      else {
        $flag = 1;
      }
    }
  }

	if (%errors) {
		return \%errors;
	}
  else {
    return 0;
  }
}