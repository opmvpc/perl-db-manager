#! /usr/bin/perl

use CGI qw(-any);
use strict;
use DBManager;
use CGI::Session;
use Helpers::HtmlHelpers;

# Template paths
my $template_js = "js/tables_sweetalerts_template.js";
my $fichier_js = "js/tables_sweetalerts.js";

# Données
my @table_names = @{DBManager::get_table_names()};

my $page = CGI->new;
my $session = new CGI::Session(undef, $page, {Directory=>'sessions'});
my $cookie = $page->cookie( -name => $session->name, -value => $session->id );

print $page->header(
             -charset=>'utf-8',
             -cookie => $cookie
            );
set_js();
set_html_head();
HtmlHelpers::set_nav_bar("tables");
set_session_message();
HtmlHelpers::set_table_html(\@table_names);
print $page->end_html();


sub set_html_head {
  print $page->start_html(-title=>"DB Manager | Liste des tables",
                          -head=>$page->meta({-name => 'viewport',-content=>'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1'}),
                          -style => { 'src' => ['styles/bootstrap.min.css', 'styles/custom.css', 'styles/sweetalert.css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'] },
                          -script=>[
                            { -type => 'text/javascript',
                              -src  => 'js/jquery.js'
                            },
                            { -type => 'text/javascript',
                              -src  => 'js/bootstrap.min.js'
                            },
                            { -type => 'text/javascript',
                              -src  => 'js/sweetalert.js'
                            },
                            { -type => 'text/javascript',
                              -src  => $fichier_js
                            },
                          ]
                        );
}

# Générer javascript des sweetalerts pour la supression d'une table
sub set_js {
  my $buffer;
  open(JS, $template_js);
  while(<JS>) {
    $buffer .= $_;
  }
  close(JS);

  open (FILEHANDLE, ">", $fichier_js) or die "Impossible d'ouvrir le fichier: $!";
  print FILEHANDLE "\$( document ).ready(function() {\n";
  foreach my $name (@table_names) {
    my $code = $buffer;
    $code =~ s/<!--cgi:table_id-->/$name/g;
    $code =~ s/<!--cgi:type-->/La table/g;
    $code =~ s/<!--cgi:delete_action-->/cible_delete_table.pl?name=$name/g;
    print FILEHANDLE $code;
  }
  print FILEHANDLE "\n});";
  close(FILEHANDLE);
}

sub set_session_message {
  # Gestion des messages en session
  if ($session->param("deleted"))
  {
    HtmlHelpers::set_session_message_html ($session->param('deleted'));
    $session->clear(["deleted"]);
  }
  if ($session->param("created"))
  {
    HtmlHelpers::set_session_message_html ($session->param('created'));
    $session->clear(["created"]);
  }
} 